﻿(function($) {

    var authentificationModule = (function () {

        var _private = {};
        var _public = {};

        _private.btnLogout_Click = function () {

            $.ajax({
                url: '/LogOut',
                type: 'POST',
                dataType: 'html',
                async: true,

                success: function (data) {
                    $('#login').replaceWith(data);
                },

                error: function (ex) {
                    console.log(ex);
                }

            });
        };

        _private.btnLogin_Click = function () {
            
            var userNameField = $('#username');
            var userPassField = $('#password');

            var user = {
                Name: userNameField.val(),
                Password: userPassField.val()
            };

            $.ajax({
                url: '/LogIn',
                type: 'POST',
                dataType: 'html',
                data: user,
                async: true,

                success: function (data) {
//                    console.log('LogIn POST Back:' + data);
                    $('#login').replaceWith(data);
                    window.location = "";
                },

                error: function(ex) {
                    console.log('LogIn POST Back ERROR:' + ex);
                }

            });
        };

        _public.init = function() {
            $('#btnLogout').on('click', _private.btnLogout_Click);
            $('#btnLogin').on('click', _private.btnLogin_Click);
        };

        return _public;
    })();


    $(function() {
        authentificationModule.init();
    });

})(jQuery);