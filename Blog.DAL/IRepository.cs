﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Blog.DAL
{
    public interface IRepository<T>
        where T: class
    {
        IQueryable<T> All { get; }
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        T Find(int id, params Expression<Func<T, object>>[] includeProperties);
        T Insert(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
