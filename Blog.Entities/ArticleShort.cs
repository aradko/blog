﻿using System;

namespace Blog.Entities
{
    public class ArticleShort
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateTime { get; set; }

        public string UserName { get; set; }

        public ArticleShort(Article a)
        {
            Id = a.Id;
            Title = a.Title;
            Text = a.Text.Length > 100 ? a.Text.Substring(0, 100) + "..." : a.Text;
            DateTime = a.DateTime;
        }   
        
        public ArticleShort(ArticleView a)
        {
            Id = a.Id;
            Title = a.Title;
            Text = a.Text.Length > 100 ? a.Text.Substring(0, 100) + "..." : a.Text;
            DateTime = a.DateTime;
            UserName = a.UserName;
        }
     }
}