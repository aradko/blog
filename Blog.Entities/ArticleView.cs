﻿using System;
using System.Collections.Generic;

namespace Blog.Entities
{
    public class ArticleView : EntityBase<int>
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
        
    }
}