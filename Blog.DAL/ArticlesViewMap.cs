﻿using System.Data.Entity.ModelConfiguration;
using Blog.Entities;

namespace Blog.DAL
{
    class ArticlesViewMap: EntityTypeConfiguration<ArticleView>
    {
        public ArticlesViewMap()
        {
            this.ToTable("ArticlesView");
            this.HasKey(it => it.Id);
        }
    }
}
