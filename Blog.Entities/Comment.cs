﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.Entities
{
    public class Comment : EntityBase<int>
    {
        public int ArticleId { get; set; }
        public int UserId { get; set; }
        public string Text { get; set; }
        public DateTime DateTime { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}