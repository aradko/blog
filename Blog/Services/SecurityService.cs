﻿using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.WebPages;
using Blog.DAL;
using Blog.Entities;

namespace Blog.Services
{
    public class SecurityService : ISecurityService
    {
        readonly IRepository<User> _usersRepository;

        public SecurityService(IRepository<User> usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public void Login(User user)
        {
            FormsAuthentication.SetAuthCookie(user.Name, true);
            HttpContext.Current.Session["UserId"] = user.Id;
        }

        public IPrincipal IsAuthentificated(IPrincipal currentPrincipal)
        {
            return currentPrincipal != null && currentPrincipal.Identity.IsAuthenticated
                ? new GenericPrincipal(currentPrincipal.Identity, new[] {"user"}) 
                : null;
        }

        public int GetSessionUserId()
        {
            return HttpContext.Current.Session["UserId"] != null
                ? HttpContext.Current.Session["UserId"].ToString().AsInt()
                : default(int);
        }

        public string GetCurrentUserName()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}