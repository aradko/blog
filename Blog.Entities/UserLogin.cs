﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.Entities
{
    public class UserLogin : EntityBase<int>
    {
        [MaxLength(20)]
        [MinLength(5)]
        [Required(ErrorMessage = "Please enter your Name. Min characters - 5. Max characters - 20.")]
        public string Name { get; set; }

        [MaxLength(20)]
        [Required(ErrorMessage = "Please enter your Password. Max characters - 20.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsAdmin { get; set; }
    }
}