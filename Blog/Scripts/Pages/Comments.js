﻿(function($) {

    var commentsModule = (function () {

        var _private = {};
        var _public = {};

        _private.btnAddComment_Click = function () {

            var commentTextField = $('#taCommentText');
            var atricleIdField = $('#hArticleId');

            var comment = {
                Text: commentTextField.val(),
                ArticleId: atricleIdField.val()
            };

            $.ajax({
                url: '/AddComment',
                type: 'POST',
                dataType: 'html',
                async: true,
                data: comment,

                success: function (data) {
//                    console.log('AddComment POST back:' + data);
                    $('#comments').append(data);
                    commentTextField.val('');
                },

                error: function (ex) {
                    console.log('AddComment POST back ERROR:' + ex);
                }

            });
        };

        _public.init = function() {
            $('#btnAddComment').on('click', _private.btnAddComment_Click);
        };

        return _public;
    })();


    $(function() {
        commentsModule.init();
    });

})(jQuery);