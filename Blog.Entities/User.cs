﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.Entities
{
    public class User : EntityBase<int>
    {
        [MaxLength(20)]
        [MinLength(5)]
        [Required(ErrorMessage = "Please enter your Name. Min characters - 5. Max characters - 20.")]
        public string Name { get; set; }

        [MaxLength(20)]
        [Required(ErrorMessage = "Please enter your Password. Max characters - 20.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [NotMapped]
        [MaxLength(20)]
        [Required(ErrorMessage = "Please repeate your Password. Max characters - 20.")]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        public bool IsAdmin { get; set; }
    }
}