﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Blog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Home",
                url: "",
                defaults: new { controller = "Article", action = "Articles" }
            );

            routes.MapRoute(
                name: "NewArticle",
                url: "NewArticle",
                defaults: new { controller = "Article", action = "NewArticle" }
                );    
            
            routes.MapRoute(
                name: "Register",
                url: "Register",
                defaults: new { controller = "User", action = "Register" }
                );   
            
            routes.MapRoute(
                name: "AddComment",
                url: "AddComment",
                defaults: new { controller = "Article", action = "AddComment" }
                );

            routes.MapRoute(
              name: "LogIn",
              url: "LogIn",
              defaults: new { controller = "User", action = "LogIn" }
              ); 
            
            routes.MapRoute(
              name: "LogOut",
              url: "LogOut",
              defaults: new { controller = "User", action = "LogOut" }
              ); 

            routes.MapRoute(
                name: "Article",
                url: "{controller}/{id}",
                defaults: new { controller = "Article", action = "Article", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
