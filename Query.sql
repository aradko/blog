
CREATE DATABASE blog_db;
USE blog_db;

Drop TABLE Comments;
Drop TABLE Articles;

CREATE TABLE Users(
	Id INT PRIMARY KEY IDENTITY(1, 1),
	Name VARCHAR(20) NOT NULL,
	IsAdmin BIT DEFAULT 0
);


CREATE TABLE Articles(
	Id INT PRIMARY KEY IDENTITY(1, 1),
	UserId INT FOREIGN KEY REFERENCES Users(Id),
	Title VARCHAR(250) NOT NULL,
	Text TEXT NOT NULL,
	DateTime DATETIME
);

CREATE TABLE Comments(
	Id INT PRIMARY KEY IDENTITY(1, 1),
	ArticleId INT FOREIGN KEY REFERENCES Articles(Id),
	UserId INT FOREIGN KEY REFERENCES Users(Id),
	Text VARCHAR(250) NOT NULL,
	DateTime DATETIME
);


INSERT INTO Users(Name, IsAdmin)
VALUES ('Admin', 1), 
	('guest', 0);

INSERT INTO Articles (UserId, Title, Text, DateTime)
VALUES 
(1,
'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis non eleifend leo. Nam fringilla est dignissim diam consectetur, dignissim placerat est lacinia. Sed tristique faucibus est, sed porttitor arcu volutpat id. Phasellus purus mi, ultricies nec mollis eget, mattis facilisis quam. Donec sollicitudin nibh vel rhoncus semper. Aliquam viverra lorem quam, nec vehicula metus imperdiet eu. Cras vitae augue ultrices, imperdiet sem a, feugiat nunc. Duis urna magna, blandit eget lectus et, mollis hendrerit dui. Cras sed lectus eget justo scelerisque faucibus. In ac lacinia leo. Aliquam sapien lectus, varius egestas iaculis quis, auctor hendrerit neque. Morbi sodales in turpis venenatis luctus. Vivamus porttitor volutpat tortor, sed sodales neque tincidunt quis. Aenean at rhoncus turpis, vitae vestibulum sapien. Duis consectetur lectus ut felis auctor sodales ac quis turpis. In hac habitasse platea dictumst.
Maecenas eu luctus augue. In id malesuada sem, nec lobortis mauris. Phasellus aliquet, orci sed laoreet feugiat, purus libero volutpat ipsum, eu aliquet est neque in eros. Mauris commodo volutpat elit at porttitor. Nam eget risus nec velit sollicitudin dictum eu vel arcu. Morbi feugiat purus vitae nisl sagittis, nec ultricies lorem consequat. Sed vulputate lectus non augue congue tristique. Aliquam accumsan in mauris quis posuere. Suspendisse interdum, dolor ac dapibus dapibus, odio magna varius justo, sed dignissim nulla quam eget massa. Pellentesque posuere facilisis lorem, consequat vehicula justo consequat et. Cras placerat mi velit, eget interdum libero fermentum eu. Nunc mauris elit, posuere ut hendrerit sed, laoreet sed tortor.
In id bibendum libero. Nam feugiat felis vel risus aliquet, in bibendum purus convallis. Vestibulum rhoncus tempor rhoncus. Duis vitae tincidunt tortor. Nunc tempor eleifend dui, at fringilla risus. Aliquam et sem nibh. Praesent placerat semper gravida. Nam euismod mauris non facilisis aliquet. Pellentesque tincidunt in justo nec auctor.
Fusce vulputate lacus metus, quis venenatis leo egestas eget. Cras tempor feugiat massa aliquet fermentum. Nunc suscipit hendrerit sapien, sit amet tempus enim blandit sit amet. Sed posuere, ipsum ut blandit fringilla, nunc sem rhoncus ligula, volutpat suscipit leo diam eu arcu. In et fermentum mauris. Integer consectetur blandit ligula. Nulla commodo augue a dignissim malesuada. Donec quis dolor accumsan, hendrerit ex porttitor, finibus tellus. Praesent sed urna cursus, sagittis sem tincidunt, interdum nunc. Integer eget sollicitudin metus, sed iaculis odio. Proin malesuada ut ligula eu commodo. Mauris nec mi luctus ligula rutrum varius. Duis efficitur a urna eu ullamcorper. Praesent porttitor leo ut arcu aliquet, nec mattis orci hendrerit. Pellentesque ut arcu ipsum. Etiam faucibus, purus in viverra congue, eros felis vestibulum felis, a finibus neque dolor a massa.
Nam sit amet justo sed ligula porttitor molestie. Curabitur non fringilla libero. Quisque ultrices quam a elit vehicula fermentum. Quisque urna ante, cursus imperdiet sodales sed, faucibus quis mauris. Sed sit amet nulla vel ante hendrerit varius. Maecenas ultrices tellus in vulputate lacinia. Fusce vitae arcu maximus, finibus turpis et, tempor dolor. Integer volutpat sed risus imperdiet sagittis.', 
GETDATE()),
(1,
'There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac dapibus arcu. Quisque sit amet mattis nunc. Cras pharetra, nibh vitae auctor lobortis, arcu dui varius metus, quis eleifend nisl neque ut risus. Aenean in est ipsum. Cras nec iaculis ligula. Aliquam posuere metus nec diam eleifend, sit amet semper tellus sodales. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc mattis tincidunt orci. Quisque ornare nunc vitae justo placerat aliquet. Aenean pharetra pharetra risus, pretium luctus tellus ullamcorper placerat. Phasellus fermentum ante sit amet enim viverra lobortis. Morbi rutrum interdum tristique. In sit amet lorem purus. Phasellus accumsan pellentesque consequat. Fusce porta dignissim nibh et tincidunt.
Nam ac mi nec elit tincidunt fermentum eu eget quam. Aliquam sed egestas mi, vel ultrices erat. Sed scelerisque, magna vitae feugiat posuere, lacus ante porttitor tellus, nec egestas metus lectus sit amet dolor. Fusce et mauris mi. Maecenas sodales mi mollis nisi tempus cursus. Morbi urna quam, pellentesque nec blandit ut, facilisis quis justo. Fusce vitae nulla sodales, volutpat sapien quis, dignissim magna. Aenean et dictum ligula, eu vulputate mauris. Praesent sodales tincidunt mauris. Aliquam erat volutpat.
Maecenas porta dolor velit, et bibendum erat venenatis vel. Praesent urna tortor, tempus eget fringilla vitae, lacinia dictum enim. Donec lobortis sollicitudin sapien sit amet dignissim. Praesent tincidunt, quam et rutrum ultrices, erat mauris efficitur massa, non bibendum sapien arcu et tellus. Phasellus urna neque, sollicitudin vel luctus vitae, aliquet vitae purus. Mauris sollicitudin efficitur cursus. Mauris venenatis ultrices tellus, vel scelerisque ante commodo a. Etiam vestibulum imperdiet libero, eu tempus eros consequat id. Ut dui orci, commodo et finibus non, faucibus in mauris. Quisque sed lobortis dolor. Curabitur et tellus non turpis rutrum varius. Duis tincidunt fermentum nunc, non sodales est congue sit amet.
Morbi venenatis libero sit amet libero sollicitudin facilisis sagittis vel leo. Nullam ac turpis neque. Aliquam a ullamcorper eros. Donec venenatis lacus a lorem suscipit posuere. Sed eu sem feugiat, auctor nisi sit amet, ornare est. Nunc feugiat libero at sodales tempor. Nam et sem eros. Sed tincidunt lorem ac lobortis pretium. Aliquam nulla sem, congue at leo et, ullamcorper ullamcorper odio. Sed eros augue, mattis eget odio eget, volutpat ornare velit. Sed id elit eget neque bibendum congue. Pellentesque eget lectus vel turpis pulvinar rutrum sit amet sed augue.', 
GETDATE())
;

INSERT INTO Comments (ArticleId, UserId, Text, DateTime)
VALUES 
(1, 1, 'Maecenas porta dolor velit, et bibendum erat venenatis vel', GETDATE()),
(1, 2, 'libero sollicitudin facilisis sagittis vel leo.', GETDATE())
;

DROP VIEW ArticlesView;

CREATE VIEW ArticlesView AS
SELECT 
	a.Id,
	u.Name AS UserName,
	a.UserId,
	a.DateTime,
	a.Title,
	a.Text
FROM Articles a
LEFT JOIN Users u on a.UserId = u.Id
;