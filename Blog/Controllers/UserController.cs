﻿using System.Linq;
using System.Web.Mvc;
using Blog.DAL;
using Blog.Entities;
using Blog.Services;

namespace Blog.Controllers
{
    public class UserController : Controller
    {
        readonly ISecurityService _securityService;
        readonly IRepository<User> _userRepository;

        public UserController(ISecurityService securityService, IRepository<User> userRepository)
        {
            _securityService = securityService;
            _userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View("RegisterView");
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            if (!ModelState.IsValid)
                return View("RegisterView");

            if (user.Password != user.ConfirmPassword)
            {
                ModelState.AddModelError("", "Passwords mismach.");
                return View("RegisterView");
            }

            if (_userRepository.All.Any(it => it.Name == user.Name))
            {
                ModelState.AddModelError("", "User is already exist.");
                return View("RegisterView");
            }

            _userRepository.Insert(user);
            return RedirectToAction("Articles", "Article");
        }

        [HttpPost]
        public ActionResult LogIn(UserLogin user)
        {
            if (ModelState.IsValid)
            {
                var dbUser = _userRepository.All.FirstOrDefault(it => it.Name == user.Name && it.Password == user.Password);

                if (dbUser != null)
                    _securityService.Login(dbUser);
            }

            return RedirectToAction("Articles", "Article");
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            _securityService.Logout();

            return RedirectToAction("Articles", "Article");
        }

        public ActionResult GetLoginView()
        {
            var user = _securityService.IsAuthentificated(HttpContext.User);

            ViewBag.IsAuthentificated = user != null && user.Identity.IsAuthenticated;
            ViewBag.UserName = user != null ? user.Identity.Name : string.Empty;

            return PartialView("LoginView");
        }
    }
}