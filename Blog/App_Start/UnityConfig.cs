using System.Web.Mvc;
using Blog.DAL;
using Blog.Entities;
using Blog.Services;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace Blog
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IRepository<Article>, Repository<Article>>();
            container.RegisterType<IViewRepository<ArticleView>, ArticlesViewRepository>();
            container.RegisterType<IRepository<Comment>, Repository<Comment>>();
            container.RegisterType<IRepository<User>, Repository<User>>();

            container.RegisterType<ISecurityService, SecurityService>();
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}