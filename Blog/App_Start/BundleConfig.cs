﻿using System.Web.Optimization;

namespace Blog
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var jsBundle = new ScriptBundle("~/bundles/js");

            jsBundle.Include("~/Scripts/Libs/jquery-2.1.3.js");
            jsBundle.Include("~/Scripts/Libs/bootstrap.js");
            jsBundle.Include("~/Scripts/Pages/ArticlePage.js");
            jsBundle.Include("~/Scripts/Pages/Comments.js");
            bundles.Add(jsBundle);


            var cssBundle = new StyleBundle("~/bundles/css");

            cssBundle.IncludeDirectory("~/Styles", "*.css", true);
            bundles.Add(cssBundle);
        }
    }
}