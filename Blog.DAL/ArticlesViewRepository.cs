﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Blog.Entities;

namespace Blog.DAL
{
    public class ArticlesViewRepository : IViewRepository<ArticleView>
    {
        private readonly BlogDbContext _dbContext;

        public ArticlesViewRepository()
        {
            var connection = ConfigurationManager.ConnectionStrings["Dev"];
            _dbContext = new BlogDbContext(connection.ToString());
        }

        public IQueryable<ArticleView> All {
            get { return _dbContext.ArticlesView; } 
        }

        public IQueryable<ArticleView> AllIncluding(params Expression<Func<ArticleView, object>>[] includeProperties)
        {
            IQueryable<ArticleView> query = _dbContext.ArticlesView;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public ArticleView Find(int id, Expression<Func<ArticleView, object>>[] includeProperties)
        {
            IQueryable<ArticleView> query = _dbContext.ArticlesView.Where(el => el.Id == id);

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.FirstOrDefault();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
