﻿(function($) {

    var articlePage = (function() {

        var _private = {};
        var _public = {};


        _public.init = function() {
            var articleId = parseInt($('#hArticleId').val());

            $('#btnNextArticle').attr("href", '/Article/' + (articleId + 1));
            $('#btnPrevArticle').attr("href", '/Article/' + (articleId - 1));
        };

        return _public;
    })();

    $(function() {
        articlePage.init();
    });

})(jQuery);