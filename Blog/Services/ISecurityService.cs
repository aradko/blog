﻿using System.Security.Principal;
using System.Web.UI.WebControls;
using Blog.Entities;

namespace Blog.Services
{
    public interface ISecurityService
    {
        void Login(User user);
        IPrincipal IsAuthentificated(IPrincipal currentPrincipal);
        int GetSessionUserId();
        string GetCurrentUserName();
        void Logout();
    }
}
