﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Blog.DAL;
using Blog.Entities;
using Blog.Services;

namespace Blog.Controllers
{
    public class ArticleController : Controller
    {
        readonly IRepository<Article> _articlesRepository;
        readonly IViewRepository<ArticleView> _articlesViewRepository;
        readonly IRepository<Comment> _commentsRepository;
        readonly ISecurityService _securityService;

        readonly Func<ArticleView, ArticleShort> _toShort = (a) => new ArticleShort(a);

        public ArticleController(IViewRepository<ArticleView> articlesViewRepository, IRepository<Article> articlesRepository, IRepository<Comment> commentsRepository, ISecurityService securityService)
        {
            _articlesViewRepository = articlesViewRepository;
            _articlesRepository = articlesRepository;
            _commentsRepository = commentsRepository;
            _securityService = securityService;
        }

        [HttpGet]
        public ActionResult Articles()
        {
            var articles = _articlesViewRepository.All.Select(_toShort).ToList();

            return View("Articles", articles);
        }

        [HttpGet]
        public ActionResult Article(int id)
        {
            var article = _articlesRepository.Find(id, it => it.Comments);

            return article != null ? View("Article", article) : View("ArticleNotFound");
        }

        [HttpGet]
        public ActionResult NewArticle()
        {
            return View("New");
        }   
        
        [Authorize]
        [HttpPost]
        public ActionResult NewArticle(Article article)
        {
            article.DateTime = DateTime.Now;
            article.UserId = _securityService.GetSessionUserId(); 

            var newArticle = _articlesRepository.Insert(article);

            return View("Article", newArticle);
        }


        [HttpPost]
        public ActionResult AddComment(Comment comment)
        {
            comment.UserId = _securityService.GetSessionUserId();
            comment.DateTime = DateTime.Now;

            var savedComment = _commentsRepository.Insert(comment);
            savedComment.User = new User { Id = savedComment.UserId, Name = _securityService.GetCurrentUserName()};

            return PartialView("Comment", savedComment);
        }
    }
}