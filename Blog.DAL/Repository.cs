﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Blog.Entities;

namespace Blog.DAL
{
    public class Repository<T> : IRepository<T>, IDisposable
        where T : EntityBase<int>
    {
        private readonly DbContext _dbContext;

        public Repository()
        {
            var connection = ConfigurationManager.ConnectionStrings["Dev"];
            _dbContext = new BlogDbContext(connection.ToString());
        }

        public IQueryable<T> All {
            get { return _dbContext.Set<T>(); } 
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public T Find(int id, Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(el=>el.Id == id);

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.FirstOrDefault();
        }

        public T Insert(T entity)
        {
           var newItem = _dbContext.Set<T>().Add(entity);
           _dbContext.SaveChanges();

           return newItem;
        }
        
        public void Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var employee = _dbContext.Set<T>().Find(id);
            _dbContext.Set<T>().Remove(employee);
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
