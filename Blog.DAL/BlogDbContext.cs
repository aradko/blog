﻿using System.Configuration;
using System.Data.Entity;
using Blog.Entities;

namespace Blog.DAL
{
    public class BlogDbContext : DbContext
    {
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleView> ArticlesView { get; set; }

        public BlogDbContext(string connection) : base(connection) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ArticlesViewMap());
        }
    }

}
